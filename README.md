# Discord Bot for PTCG World 

## A discord bot created by discord.js
<br />

# Init
Create config.json
Demo: 

    {
        "token": "enter-your-bot-token-here",
        "clientId": "enter-your-bot-client-Id-here"
    }

Npm install

    npm install

Deploy command to all server

    npm run deploy-commands

# Run in local
Start with production mode

    npm start

Start with development mode

    npm run dev

# Run in Docker
Build 

    docker build -t ptcg-discord-bot .

Run

    docker run ptcg-discord-bot

Docket compose

    docker compose up