const { SlashCommandBuilder, PermissionFlagsBits, ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder } = require('discord.js');
const { v4: uuidv4 } = require('uuid');

let _execute = async (interaction) => {
    await interaction.reply({ content: 'Autocomplete!' });
}

let _autocomplete = async (interaction) => {
    const focusedOption = interaction.options.getFocused(true);
    let choices;

    if (focusedOption.name === 'name') {
        choices = ['faq', 'install', 'collection', 'promise', 'debug'];
    }

    if (focusedOption.name === 'theme') {
        choices = ['halloween', 'christmas', 'summer'];
    }

    const filtered = choices.filter(choice => choice.startsWith(focusedOption.value));
    interaction.respond(
        filtered.map(choice => ({ name: choice, value: choice })),
    );
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('autocomplete')
        .setDescription('String input with autocomplete')
        .addStringOption(option =>
            option.setName('name')
                .setDescription('Enter your name')
                .setAutocomplete(true))
        .addStringOption(option =>
            option.setName('theme')
                .setDescription('Enter your theme')
                .setAutocomplete(true))
    ,
    async execute(interaction) {
        try {
            await _execute(interaction)
        } catch (error) {
            console.error(error);
            interaction.reply({ content: `There was an error while executing this command!\n${error}`, ephemeral: true });
        }
    },
    async autocomplete(interaction) {
        try {
            await _autocomplete(interaction)
        } catch (error) {
            console.error(error);
            interaction.reply({ content: `There was an error while executing this command!\n${error}`, ephemeral: true });
        }
    },
};