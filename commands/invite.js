const { ContextMenuCommandBuilder, ApplicationCommandType } = require('discord.js');

let _execute = async (interaction) => {
    // Get the User's username from context menu
    const inviter = interaction.user;
    const invitee = interaction.targetUser;
    interaction.client.socket.emit('discord - create game room', {
        inviter: inviter,
        invitee: invitee,
    })
    await interaction.reply({ content: `${inviter} invited ${invitee} to play a game. Socket-id: ${interaction.client?.socket?.id}` });
}

module.exports = {
    data: new ContextMenuCommandBuilder()
        .setName('Invite')
        .setType(ApplicationCommandType.User)
    ,
    async execute(interaction) {
        try {
            await _execute(interaction)
        } catch (error) {
            console.error(error);
            interaction.reply({ content: `There was an error while executing this command!\n${error}`, ephemeral: true });
        }
    },
};