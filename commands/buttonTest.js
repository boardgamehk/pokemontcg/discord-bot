const { SlashCommandBuilder, PermissionFlagsBits, ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder } = require('discord.js');
const { v4: uuidv4 } = require('uuid');

let _execute = async (interaction) => {
    let uuid = uuidv4()
    let content
    let row = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
                .setCustomId(buttonUid)
                .setLabel('Primary')
                .setStyle(ButtonStyle.Primary),
        );

    await interaction.reply({ content: 'Click the button in 5 seconds!', components: [row] });

    let filter = i => i.customId === uuid

    let collector = interaction.channel.createMessageComponentCollector({ filter, time: 5000 });

    collector.on('collect', async i => {
        content = `Button ${uuid} was clicked!`
        row = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setCustomId(uuid)
                    .setLabel('Primary')
                    .setStyle(ButtonStyle.Primary)
                    .setDisabled(true),
            );
        await i.update({ content: content, components: [row] });
    });

    collector.on('end', async (collected) => {
        row = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setCustomId(buttonUid)
                    .setLabel('Primary')
                    .setStyle(ButtonStyle.Primary)
                    .setDisabled(true),
            );
        await interaction.editReply({ content: content, components: [row] });
    });
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('button')
        .setDescription('Test button function')
    ,
    async execute(interaction) {
        try {
            await _execute(interaction)
        } catch (error) {
            console.error(error);
            interaction.reply({ content: `There was an error while executing this command!\n${error}`, ephemeral: true });
        }
    },
};