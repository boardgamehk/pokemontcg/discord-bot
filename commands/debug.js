const { SlashCommandBuilder } = require('discord.js');

let _execute = async (interaction) => {
    await interaction.reply({ content: 'Debug!' });
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('debug')
        .setDescription('Debug command')
    ,
    async execute(interaction) {
        try {
            await _execute(interaction)
        } catch (error) {
            console.error(error);
            interaction.reply({ content: `There was an error while executing this command!\n${error}`, ephemeral: true });
        }
    },
};