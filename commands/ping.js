const { SlashCommandBuilder, PermissionFlagsBits } = require('discord.js');
const wait = require('node:timers/promises').setTimeout;

let _execute = async (interaction, client) => {
    await interaction.deferReply({ ephemeral: false });
    await wait(4000);
    await interaction.editReply({ content: `Websocket heartbeat: ${client.ws.ping}ms.`, ephemeral: false });
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('ping')
        .setDescription('Replies with Pong!')
    ,
    async execute(interaction, client) {
        try {
            await _execute(interaction, client);
        } catch (error) {
            console.error(error);
            interaction.reply({ content: `There was an error while executing this command!\n${error}`, ephemeral: true });
        }
    },
};