const { SlashCommandBuilder, ActionRowBuilder, ModalBuilder, TextInputBuilder, TextInputStyle } = require('discord.js');
const { v4: uuidv4 } = require('uuid');

let _execute = async (interaction) => {
    let uuid = uuidv4()
    const modal = new ModalBuilder()
        .setCustomId(`modal_${uuid}`)
        .setTitle('My Modal');

    // Add components to modal

    // Create the text input components
    const favoriteColorInput = new TextInputBuilder()
        .setCustomId('favoriteColorInput')
        // The label is the prompt the user sees for this input
        .setLabel("What's your favorite color?")
        // Short means only a single line of text
        .setStyle(TextInputStyle.Short);

    const hobbiesInput = new TextInputBuilder()
        .setCustomId('hobbiesInput')
        .setLabel("What's some of your favorite hobbies?")
        // Paragraph means multiple lines of text.
        .setStyle(TextInputStyle.Paragraph);

    // An action row only holds one text input,
    // so you need one action row per text input.
    const firstActionRow = new ActionRowBuilder().addComponents(favoriteColorInput);
    const secondActionRow = new ActionRowBuilder().addComponents(hobbiesInput);

    // Add inputs to the modal
    modal.addComponents(firstActionRow, secondActionRow);

    await interaction.showModal(modal);
}

let _modalSubmit = async (interaction) => {
	const favoriteColor = interaction.fields.getTextInputValue('favoriteColorInput');
	const hobbies = interaction.fields.getTextInputValue('hobbiesInput');
    await interaction.reply({ content: `Your submission was received successfully! (FavoriteColor:${favoriteColor} Hobbies:${hobbies})` });
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('modal')
        .setDescription('Modal command')
    ,
    async execute(interaction) {
        try {
            await _execute(interaction)
        } catch (error) {
            console.error(error);
            interaction.reply({ content: `There was an error while executing this command!\n${error}`, ephemeral: true });
        }
    },
    async modalSubmit(interaction) {
        try {
            await _modalSubmit(interaction)
        } catch (error) {
            console.error(error);
            interaction.reply({ content: `There was an error while executing this command!\n${error}`, ephemeral: true });
        }
    },
};