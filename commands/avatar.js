const { SlashCommandBuilder, PermissionFlagsBits, ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder } = require('discord.js');

let _execute = async (interaction) => {
    const user = interaction.options.getUser('target');
    if (user) return interaction.reply(`${user.username}'s avatar: ${user.displayAvatarURL({ dynamic: true })}`);
    return interaction.reply(`Your avatar: ${interaction.user.displayAvatarURL()}`);
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('avatar')
        .setDescription('Get the avatar URL of the selected user, or your own avatar.')
        .addUserOption(option => option
            .setName('target')
            .setDescription('The user\'s avatar to show')
            .setRequired(true)
        )
    ,
    async execute(interaction) {
        try {
            await _execute(interaction)
        } catch (error) {
            console.error(error);
            interaction.reply({ content: `There was an error while executing this command!\n${error}`, ephemeral: true });
        }
    },
};