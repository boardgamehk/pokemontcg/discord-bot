const { SlashCommandBuilder, PermissionFlagsBits } = require('discord.js');

let _execute = async (interaction) => {
    const input = interaction.options.getString('input');
    if (!input) {
        await interaction.reply({ content: 'You must provide an input!', ephemeral: true });
        return;
    }

    await interaction.reply(input);
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('echo')
        .setDescription('Replies with your input!')
        .addStringOption(option => option
            .setName('input')
            .setDescription('Enter a string')
            .setRequired(true)
        )
    ,
    async execute(interaction) {
        try {
            await _execute(interaction)
        } catch (error) {
            console.error(error);
            interaction.reply({ content: `There was an error while executing this command!\n${error}`, ephemeral: true });
        }
    },
};