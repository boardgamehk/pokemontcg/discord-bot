require('./config.js')
const { io } = require('socket.io-client')

const fs = require('node:fs');
const path = require('node:path');
const { Client, Collection, GatewayIntentBits } = require('discord.js');
const { token } = require('./config.json');

console.log('Discord Bot is starting...')

const client = new Client({
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.DirectMessages,
        GatewayIntentBits.MessageContent,
        GatewayIntentBits.GuildVoiceStates,
    ]
})

const eventsPath = path.join(__dirname, 'events');
const eventFiles = fs.readdirSync(eventsPath).filter(file => file.endsWith('.js'));

for (const file of eventFiles) {
    const filePath = path.join(eventsPath, file);
    const event = require(filePath);
    console.log(`Registering event: ${event.name}`);
    if (event.once) {
        client.once(event.name, (...args) => event.execute(...args));
    } else {
        client.on(event.name, (...args) => event.execute(client, ...args));
    }
}

const actionsPath = path.join(__dirname, 'actions');
const actionFiles = fs.readdirSync(actionsPath).filter(file => file.endsWith('.js'));
client._actions = new Collection();

for (const file of actionFiles) {
    const filePath = path.join(actionsPath, file);
    const action = require(filePath);
    client._actions.set(action.data.name, action);
}

// Login to Discord with your client's token
console.log('Logging in to Discord...')
client.login(token)

let socket = io.connect(process.env.SERVER_URL)
socket.on('connect', () => {
    console.log(`Connected to socket server: ${process.env.SERVER_URL}`)
    client.socket = socket

    socket.emit('registerDiscordBot')
});

socket.onAny((event, ...args) => {
    if (event === 'connect') {
        return
    }

    if (!client._actions.has(event)) {
        return
    }
    console.log(`socket event: ${event}`);
    let action = client._actions.get(event);
    action.execute(client, socket, ...args);
});