let _execute = async (client, socket, data) => {
    let config = require('../config.json')
    let fields = data.fields
    let returnData = {}
    // foreach field in fields, copy it from config and save it to returnData
    for (let i = 0; i < fields.length; i++) {
        const field = fields[i];
        returnData[field] = config[field]
    }
    socket.emit('bot config', returnData)
}

module.exports = {
    data: {
        name: 'get bot config',
    }
    ,
    async execute(client, socket, data) {
        try {
            await _execute(client, socket, data)
        } catch (error) {
            console.error(error);
        }
    },
};