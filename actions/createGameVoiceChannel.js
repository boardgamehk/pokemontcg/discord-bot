const { ChannelType } = require('discord.js');
const wait = require('node:timers/promises').setTimeout;

let _execute = async (client, socket, data) => {
    let inviter
    let invitee
    let roomName = data.roomName
    let token = data.token
    let { ptcgwordDiscordId, battleRoomCategoryId, battleStandByRoomId } = require('../config.json')
    let ptcgwordDiscord = client.guilds.cache.find(guild => guild.id === ptcgwordDiscordId)
    let channels = ptcgwordDiscord.channels

    await ptcgwordDiscord.members.fetch(data.inviterId)
        .then(member => {
            inviter = member
        })
    await ptcgwordDiscord.members.fetch(data.inviteeId)
        .then(member => {
            invitee = member
        })

    channels.create({
        name: `${roomName}`,
        type: ChannelType.GuildVoice,
        userLimit: 10,
        parent: battleRoomCategoryId,
    })
        .then(async (channel) => {
            console.log(`Created voice channel: ${roomName}`)
            let url = `http://localhost:8080/#/room?token=${token}`
            channel.send(`Please use below link to join the game.\n${url}`)

            if (inviter) {
                let runAcync = async () => {
                    if (inviter.voice.channel) {
                        let inviterDiscordTag = `${inviter.user.username}#${inviter.user.discriminator}`
                        inviter.voice.setChannel(channel)
                        console.log(`Set voice channel for inviter: ${inviterDiscordTag} to ${channel}`)
                    } else {
                        inviter.send(`Please join voice channel ${channel} for this game`)
                    }
                }
                runAcync()
            }

            if (invitee) {
                let runAcync = async () => {
                    if (invitee.user.bot) return

                    if (invitee.voice.channel) {
                        let inviteeDiscordTag = `${invitee.user.username}#${invitee.user.discriminator}`
                        invitee.voice.setChannel(channel)
                        console.log(`Set voice channel for invitee: ${inviteeDiscordTag} to ${channel}`)
                    } else {
                        invitee.send(`Please join voice channel ${channel} for this game`)
                    }
                }
                runAcync()
            }


            await wait(15000);

            if (inviter?.voice.channel) {
                let inviterDiscordTag = `${inviter.user.username}#${inviter.user.discriminator}`
                inviter.voice.setChannel(battleStandByRoomId)
                console.log(`Move inviter: ${inviterDiscordTag} to stand by room: ${battleStandByRoomId}`)
            }

            if (invitee?.voice.channel) {
                let inviteeDiscordTag = `${invitee.user.username}#${invitee.user.discriminator}`
                invitee.voice.setChannel(battleStandByRoomId)
                console.log(`Move invitee: ${inviteeDiscordTag} to stand by room: ${battleStandByRoomId}`)
            }

            await wait(1000);

            channel.delete()
        })
        .catch(error => {
            console.log(error)

            if (inviter) {
                inviter.send(`Cannot create channel\n${error}`)
            }
        });
}

module.exports = {
    data: {
        name: 'create game voice channel',
    }
    ,
    async execute(client, socket, data) {
        try {
            await _execute(client, socket, data)
        } catch (error) {
            console.error(error);
        }
    },
};