let _execute = async (client, socket, data) => {
    let inviterId = data.inviter.id
    let inviteeId = data.invitee.id
    console.log(`Creating game room for inviter: ${inviterId} and invitee: ${inviteeId}`)
    // console.log(`inviter: ${JSON.stringify(inviter)}`)
    let content = {}
    //   content.password = password
    socket.emit('create room', { content });
    socket.once('create room result', async function (data) {
        const inviter = await client.users.fetch(inviterId);
        const invitee = await client.users.fetch(inviteeId);

        if (data.result != 'success') {
            console.log(data);
            inviter.send(`Cannot create game room for you and <@${invitee.id}>\n${data.reason}`)
            return;
        }

        console.log(`Game room created: ${data.token}`)
        let url = `http://localhost:8080/#/room?token=${data.token}`
        inviter.send(`You invted <@${invitee.id}> to play a game.\nPlease use below link to join the game.\n${url}`);
        // invitee_user.send(`<@${inviter.id}> invted you to play a game.\nPlease use below link to join the game.\n${url}`);

        let action = client._actions.get(`create game voice channel`);
        action.execute(client, socket, {
            inviterId: inviter.id,
            inviteeId: invitee.id,
            roomName: data.token,
            token: data.token
        });
    })
}

module.exports = {
    data: {
        name: 'create game room by invite',
    }
    ,
    async execute(client, socket, data) {
        try {
            await _execute(client, socket, data)
        } catch (error) {
            console.error(error);
        }
    },
};