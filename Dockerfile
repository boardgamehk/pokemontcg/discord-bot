FROM node:16
COPY ./ .
RUN npm install
RUN npm update
RUN npm audit fix
CMD ["node", "index.js"]