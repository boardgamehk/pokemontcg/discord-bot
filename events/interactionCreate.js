const fs = require('node:fs');
const path = require('node:path');
const { Collection, InteractionType } = require('discord.js');

module.exports = {
    name: 'interactionCreate',
    execute(client, interaction) {
        try {
            if (interaction.isButton()) return
            
            interaction.client = client
            
            client.commands = new Collection();
            const commandsPath = path.join(__dirname, '../commands');
            const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));

            for (const file of commandFiles) {
                const filePath = path.join(commandsPath, file);
                const command = require(filePath);
                // Set a new item in the Collection
                // With the key as the command name and the value as the exported module
                client.commands.set(command.data.name, command);
            }

            let command = client.commands.get(interaction.commandName);

            if (interaction.type === InteractionType.ModalSubmit) {
                let commandName = interaction.customId.split('_')[0]
                command = client.commands.get(commandName);
                command.modalSubmit(interaction);
                return
            }

            if (!command) return;

            if (interaction.type === InteractionType.ApplicationCommandAutocomplete) {
                command.autocomplete(interaction);
                return
            }

            command.execute(interaction, client);

            console.log(`${interaction.user?.tag} in #${interaction.channel?.name} triggered ${interaction.commandName} command.`);
        } catch (error) {
            console.error(error);
            interaction.reply({ content: `There was an error while executing this command!\n${error}`, ephemeral: true });
        }
    },
};