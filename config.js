switch (process.env.NODE_ENV) {
    case 'dev':
        process.env.SERVER_URL = 'http://localhost:3000'
        break
    default:
        process.env.SERVER_URL = 'https://ptcg-socket.ptcg.world'
        break
}